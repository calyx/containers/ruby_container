ARG  RUBY_BASE_IMAGE=3.1-bookworm
FROM ruby:${RUBY_BASE_IMAGE}

RUN apt-get update && apt-get install --no-install-recommends -y \
    brotli \
    build-essential \
    busybox \
    curl \
    git \
    gzip \
    lsof \
    mariadb-client \
    nodejs \
    npm \
    postgresql-client \
    redis-tools \
    rsync \
    rustc \
    socat \
    sqlite3 \
    wkhtmltopdf \
    libfontconfig1-dev \
    libgif-dev \
    libharfbuzz-dev \
    libjansson-dev \
    libjpeg-dev \
    liblzma-dev \
    libmariadb-dev \
    libpng-dev \
    libpq-dev \
    libsodium-dev \
    libsqlite3-dev \
    libssl-dev \
    libvips-dev \
    libxext-dev \
    libxml2-dev \
    libxrender-dev \
    libyaml-dev

RUN gem update --system
